
\usepackage[fleqn]{amsmath}

Use this for equations:


\begin{eqnarray}
"result =&  "expression" \nonumber(for non-final or non-important lines) \\
"result =&  "expression" & [unit] 
\end{eqnarray}


_____________________________________________________
Followed by this for a description:

\begin{table}[H]
\begin{tabular}{l l l}
Where & & \\
&"Symbol"$ "Description" & [Unit]\\
&"Symbol"$ "Description" & [Unit]\\
\end{tabular}
\end{table}
