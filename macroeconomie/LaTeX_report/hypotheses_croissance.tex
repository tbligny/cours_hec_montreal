\documentclass[Rapport.tex]{subfiles}
\begin{document}
\chapter{Hypothèse de croissance de marché au cours des prochaines années}
\label{chap:hypotheses_croissance}

\lettrine{L}e \autoref{chap:hypotheses_croissance} portera sur certaines hypothèses de croissance en lien avec le PIB, mais aussi sur une analyse rigoureuse d'une multitude de facteurs socioéconomiques et culturels.

\section{Croissance estimée du PIB/habitant et courbe de Solow}\label{sec:croissance_pib}

Ainsi, afin de valider nos intuitions à l'égard du potentiel de marché, nous amorcerons notre analyse en nous intéressant au référent économique par excellence : le PIB. En effet, premier jalon de toute analyse macroéconomique, l'analyse de l'évolution du PIB nous permet de bien cerner le potentiel de croissance d'un marché cible.

\begin{wrapfigure}{r}{0.7\textwidth}
		\includegraphics[width=0.67\textwidth]{images/2/5}
		\caption{Maurice : Évolution du PIB/composante (millions \$) \\2014-2018}
		\vspace{-5mm}
		\label{wrap-fig:5}
\end{wrapfigure} 

Le PIB de chaque année est évalué par la formule $PIB = C + I + G + Nx$, où $C$ représente la consommation, $I$ l'investissement, $G$ les Dépenses du gouvernement et $Nx$ les exportations nettes. Les \autoref{wrap-fig:5} et \autoref{fig:6_7} nous donnent la répartition du PIB de l'île Maurice, et le portrait est sans équivoque : la consommation est au cœur des revenus. Par ailleurs, le $Nx$ est négatif et relativement élevé. C'est donc dire qu'il s'agit d'un grand importateur (relativement aux exportations). La croissance du PIB dépend notamment de la croissance de la productivité des facteurs (fonction de Cobb-Douglas) où il s'agit de mettre en relation le taux d'emploi $\%\Delta\frac{N}{Pop}$, le taux de productivité total des facteurs $\Delta A$, et le taux du ratio du capital par travailleur $0,3 \% \Delta \frac{K}{N}$. Les prochaines sections nous permettront d'estimer l'évolution anticipée de ces ratios.

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/2/6}
                \caption{Maurice : PIB/composante -- 2017}
                \label{fig:6}
        \end{subfigure}%
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/2/7}
                \caption{Maurice : PIB/composante -- 2018 Q1--Q3}
                \label{fig:7}
        \end{subfigure}
        %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
        \caption{Maurice : PIB/composante -- 2017 vs. 2018}
        \label{fig:6_7}
\end{figure}

\begin{wrapfigure}{r}{0.7\textwidth}
		\includegraphics[width=0.67\textwidth]{images/2/8}
		\caption[Maurice vs. le monde : PIB/habitant -- 2015--2019 et projections sur 5 ans]{Maurice, Afrique, Pays émergents, Économies avancées, Canade et le monde : PIB/habitant -- 2015--2019 et projections sur 5 ans}
		%\vspace{-5mm}
		\label{wrap-fig:8}
\end{wrapfigure} 

La \autoref{wrap-fig:8} démontre que l'économie de l'Ïle Maurice se porte généralement bien, que le pays connaît une croissance constante depuis 2015 et que cette belle embellie devrait se maintenir de 2019 à 2024. En effet, la \autoref{wrap-fig:8} permet de constater une croissance annuelle du PIB de $4\%$, un rendement supérieur à la moyenne mondiale et surtout bien au-dessus des rendements des économies avancées\footnote{AUT, BEL, CAN, CHE, CZE, DEU, DNK, ESP, EST, FIN, FRA, GBR, GRC, HKG, IRL, ISR, ITA, JPN, KOR, LTU, LUX, LVA, NLD, NOR, PRT, SGP, SVK, SVN, SWE, TWN, USA}. Qui plus est, l'île Maurice a obtenu un rendement supérieur au reste de l'Afrique ces dernières années et on peut s'attendre à ce que le rendement du PIB mauricien soit supérieur pour encore au moins deux ans. En fait, la croissance du PIB devrait tourner autour de $4\%$ de 2021 à 2024, une cible anticipée en parfaite adéquation avec les pays émergents, groupe auquel appartient l'île Maurice. Ainsi, bien que ces chiffres à eux seuls ne fassent pas nécessairement de l'île Maurice \emph{le} marché, il y a déjà là des signes annonciateurs d'un potentiel à saisir, comme le confirment les tendances économiques de la \autoref{fig:9}.

\begin{figure}[H]
\centering
	\includegraphics[width=\textwidth]{images/2/9}
    \caption[Maurice : Indicateurs économiques]{Maurice : Indicateurs économiques (PIB, PIB/hab, croissance, balance commerciale et composition du PIB) \\ Source : \url{https://unctadstat.unctad.org/CountryProfile/GeneralProfile/fr-FR/480/index.html}}
    %UNCTADstat - Profil général
\label{fig:9}
\end{figure}

\begin{wrapfigure}{r}{0.7\textwidth}
		\includegraphics[width=0.67\textwidth]{images/2/10}
		\caption{Croissance du PIB/hab (USD courants) \\ 1980--2020 et ligne de projection}
		\vspace{-5mm}
		\label{wrap-fig:10}
		%https://www.imf.org/external/datamapper/NGDPDPC@WEO/OEMDC/MUS/SSQ
\end{wrapfigure} 
De manière plus spécifique, le PIB/habitant est particulièrement évocateur afin de pouvoir évaluer s'il y a un potentiel de marché. À cet effet, cet indicateur pour l'île Maurice reflète bien l'histoire du succès
économique puisqu'il est possible d'observer sur la \autoref{wrap-fig:10} une augmentation de plus de $300\%$ en une quarantaine d'années. L'indicateur est particulièrement éloquent quand on le compare avec la moyenne de l'Afrique subsaharienne qui est demeurée faible et sans réelle croissance au cours de la même période. Le constat demeure tout aussi impressionnant quand on compare Maurice avec les économies émergentes. En effet, bien que les économies émergentes connaissent une embellie depuis le tournant des années 2000, cette croissance accuse encore bien du retard sur celle de l'île Maurice. En fait, à ce jour, Maurice détient l'un des PIB/hab. les plus élevés du continent\footnote{\url{https://www.courrierinternational.com/article/ile-maurice-un-miracle-economique-qui-fete-ses-40-ans}}

\section{Croissance : facteurs d'influence}\label{sec:croissance_influence}

Maintenant que nous avons établi un portrait général de l'île Maurice et du potentiel du marché, il convient désormais de se pencher sur les différents attributs du contexte socioéconomique et géopolitique afin de moduler la recommendation d'investissement de \textsc{BeerHaven}.

\subsection{Démographie}\label{subsec:demographie}


\begin{wrapfigure}{r}{0.7\textwidth}
		\includegraphics[width=0.67\textwidth]{images/2/11}
		\caption[Indicateurs démographiques et économiques clés -- 2017]{Indicateurs démographiques et économiques clés -- 2017 \\ \url{https://unctadstat.unctad.org/CountryProfile/GeneralProfile/fr-FR/480/index.html}}
		\label{wrap-fig:11}
		    %UNCTADstat - Profil général
\end{wrapfigure} 
Comme mentionné dans le \autoref{chap:intro} ainsi que sur la \autoref{wrap-fig:11}, l'île Maurice compte une population de 1,264M d'habitants qui s'accroît naturellement de $0,1\%$/année. Près de la moitié de la population se situe en zone urbaine ($40,8\%$). L'âge moyen des Mauriciens est de 31 ans et l'espérance de vie est de 71,2 ans pour les hommes et de 77,8 ans pour les femmes. 610 400 habitants font partie de la population
active\footnote{CIA - The World Factbook, 2015} et le taux d'activité est de $60,3\%$\footnote{OIT, Laborstat - Yearly Statistics, 2017}.


De ce fait, les taux de scolarisation ont augmenté, l'espérance de vie est passée de 62 ans en 1970 à 74,6 ans aujourd'hui. La mortalité infantile est passée de 64 pour 1 000 naissances vivantes en 1970 à 10 et le coefficient de Gini est passé de 0,50 en 1962 à 0,36. En 2018, Maurice a été classée au premier rang en Afrique pour son indice de développement humain et 65$^\text{e}$ sur 189 pays dans le monde\footnote{\url{http://hdr.undp.org/en/2018-update}}. Les indicateurs démographiques sont donc tous à la hausse, ce qui nous permet d'anticiper des conditions de développement économique avantageuses qui impacteront positivement la fonction de productivité.

\subsection{R\&D, innovation et productivité}\label{subsec:rd_innovation}

À ce titre, en matière de R\&D, d'innovation et de productivité, certains indicateurs sont positifs, mais on dénote un retard important pour l'ensemble des pays de l'Afrique subsaharienne. 
Plus spécifiquement, l'île Maurice se trouve en retrait des économies mondiales dans ce domaine, tant en fonction du nombre de chercheurs/million d'habitants, qu'en fonction des investissements en R\&D en pourcentage du PIB.

Malgré ce retard à l'échelle mondiale, des indicateurs spécifiques à l'île Maurice témoignent d'une volonté évidente de provoquer un retournement de situation en la matière. Ainsi, les chercheurs mauriciens sont passés de moins de 300 en 2012, à près de 1 500 en 2017 (voir \autoref{wrap-fig:12}), une croissance de près de $500\%$ en 5 ans. Qui plus est, cette croissance s'est accompagnée d'une présence paritaire des femmes au sein de la communauté de chercheurs mauriciens (voir \autoref{wrap-fig:13}).

\newpage

\begin{figure}[H]
\centering
		\includegraphics[width=\textwidth]{images/2/12}
		\caption[Nombre de chercheurs et investissements en R\&D/hab -- 2017]{Nombre de chercheurs et investissements en R\&D/hab -- 2017\\\url{http://uis.unesco.org/apps/visualisations/research-and-development-spending/}}
		\vspace{-5mm}
		\label{wrap-fig:12}
		%http://uis.unesco.org/apps/visualisations/research-and-development-spending/
\end{figure} 


\begin{wrapfigure}{r}{0.4\textwidth}
		\includegraphics[width=0.37\textwidth]{images/2/13}
		\caption[Croissance du nombre de chercheurs et part de femmes]{Croissance du nombre de chercheurs et part de femmes :  \url{http://uis.unesco.org/en/country/mu?theme=science-technology-and-innovation}}
		\vspace{-40mm}
		\label{wrap-fig:13}
\end{wrapfigure} 

Au même titre, quand on compare 2012 avec 2017 en matière de pourcentage du PIB réservé en investissement en R\&D, il est possible d'observer l'orchestration d'un changement. En effet le pourcenatge a doublé entre 2012 et 2017 en passant de $0,2\%$ à $0,4\%$ du PIB réservé à des dépenses en R\&D. Toutes ces initiatives combinées avec le retard actuel permettent d'estimer qu'il s'agit d'un moment charnière dans l'économie mauricienne et que c'est maintenant qu'il faut investir et s'y implanter.

La \autoref{fig:14_15} permet d'observer que c'est sous l'impulsion gouvernementale que les investissements en R\&D s'organisent puisque près de $80\%$ de ceux-ci proviennent du gouvernement. Parallèlement, la \autoref{fig:15} démontre que l'île Maurice est de plus en plus attrayante pour les investissements étrangers. Cette source de financement a vu sa part s'accroître de $6,4\%$ en 2012 à $12,30\%$ de l'ensemble des investissements en R\&D en 2017. Une telle croissance n'est pas étrangère à l'attrait économique grandissant de l'endroit.

\newpage

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.23\textwidth}
                \includegraphics[width=\textwidth]{images/2/14}
                \caption{GERD en \% du PIB}
                \label{fig:14}
        \end{subfigure}%
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.72\textwidth}
                \includegraphics[width=\textwidth]{images/2/15}
                \caption{GERD par source de financement}
                \label{fig:15}
        \end{subfigure}
        %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
        \caption[Dépenses brutes domestiques en R\&D (GERD)]{Dépenses brutes domestiques en R\&D (GERD) : \url{http://uis.unesco.org/en/country/mu?theme=science-technology-and-innovation}}
        \label{fig:14_15}
\end{figure}


\begin{wrapfigure}{l}{0.7\textwidth}
		\includegraphics[width=0.67\textwidth]{images/2/16}
		\caption[Productivity trends of the Manufacturing Sector, 2014--2018]{Productivity trends of the Manufacturing Sector, 2014--2018 : \url{http://uis.unesco.org/apps/visualisations/research-and-development-spending/}}
		\vspace{-5mm}
		\label{wrap-fig:16}
\end{wrapfigure} 
Enfin, en matière de productivité, il convient de remarquer l'indicateur "Unit labour cost index (ULC)" qui s'explique comme suit : \emph{"Unit labour cost is the remuneration of labour (compensation of employees) to produce one unit of output. It is computed as the ratio of the labour cost index to an index of production. The index shows the rate of change in labour cost per unit of output."} On observe sur la \autoref{wrap-fig:16} une évolution passant de 118 à 125 entre les années 2014 et 2018, soit une croissance de $6\%$ des coûts de production par unité sur 4 ans, une croissance largement inférieure à l'inflation au cours de la même période. Il est donc possible de conclure que les Mauriciens sont aujourd'hui plus productifs qu'ils ne l'étaient en 2014. Ce facteur explique en grande partie l'évolution positive anticipée du PIB mauricien par le FMI au cours des prochaines années.


\subsection{Capital humain}
\label{subsec:capital_humain}

La \autoref{fig:17} illustre l'efficacité du capital humain\footnote{Le nouvel indice de la Banque Mondiale cherche à mesurer l'investissement des pays dans le capital humain, qui se définit comme étant l'ensemble des connaissances, des compétences et des conditions de santé que les personnes acquièrent au cours de leur vie (\url{https://www.lafinancepourtous.com/2018/10/17/le-nouvel-indice-du-capital-humain/\#targetText=Mesurer\%20l'investissement\%20dans\%20la,au\%20cours\%20de\%20leur\%20vie})} de l'île Maurice. Concrètement, cet indice permet d'évaluer les chances de survie, de bonne éducation et de bonne santé des enfants nés aujourd'hui lorsqu'ils auront atteint l'âge de travailler. Cet indice est pertinent, car il permet d'anticiper le portrait de la main-d'œuvre (quantité et qualité) disponible dans les années futures. L'île Maurice fait figure avantageuse en se positionnant dans le premier tiers (52\textsuperscript{e} sur 157 pays\footnote{\url{https://databank.worldbank.org/data/download/hci/HCI_2pager_MUS.pdf}}) des pays quant à la qualité de son capital humain dans le futur (indice de 0,63). C'est une mesure qui permet d'estimer le pourcentage de chances que l'enfant né aujourd'hui réalise son plein potentiel dans le futur. Qui plus est, avec son score de 0,63 l'île Maurice est largement en avance sur le bassin des économies de l'Afrique subsaharienne. La \autoref{fig:17} présente différents pays selon leur classement en termes d'indice de capital humain et de PIB par habitant.

\begin{figure}[H]
\centering
	\includegraphics[width=\textwidth]{images/2/17}
    \caption[Indice du capital humain pour les filles et les garçons en Afrique subsaharienne]{Indice du capital humain pour les filles et les garçons en Afrique subsaharienne : \url{https://www.worldbank.org/en/publication/human-capital}}
\label{fig:17}
\end{figure}


\subsection{Commerce, échanges de biens et services avec le reste du  monde}
\label{subsec:commerce_echange}

La balance commerciale de l'île Maurice en matière de marchandises est négative depuis de nombreuses années. Cette donnée tend à s'accroître alors que les importations augmentent, pendant que les exportations de marchandises sont importantes, mais demeurent stables au fil du temps.

Les exportations sont destinées en grande partie à la France, au Royaume-Uni, aux États-Unis, à l'Afrique du Sud et à l'Italie (voir \autoref{fig:18}). Ces cinq pays représentent près de la moitié de l'ensemble des exportations mauriciennes. Ces exportations sont composées à $53\%$ de biens manufacturés et à $39\%$ de produits alimentaires.

Toutefois, au niveau des services, la balance commerciale de l'île Maurice se maintient en territoire positif, avec une balance positive de 871 M\$ en 2017 (voir \autoref{fig:19}). Les services exportés émanent en grande partie du secteur des voyages qui représente $53,8\%$ des exportations en matière de service. Le transport suit avec $23,9\%$ des exportations dans le domaine des services.


\begin{figure}[H]
        \centering
                \includegraphics[width=\textwidth]{images/2/18}
                \caption[Commerce international de marchandises]{Commerce international de marchandises : \url{https://unctadstat.unctad.org/CountryProfile/GeneralProfile/fr-FR/480/}}
                \label{fig:18}
\end{figure}%

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.6\textwidth}
                \includegraphics[width=\textwidth]{images/2/19}
                \caption{Commerce international des services}
                \label{fig:19}
        \end{subfigure}%
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{images/2/20}
                \caption{Indices du commerce}
                \label{fig:20}
        \end{subfigure}
        %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
        \caption[Commerce à Maurice]{Commerce à Maurice : \url{https://unctadstat.unctad.org/CountryProfile/GeneralProfile/fr-FR/480/}}\label{fig:19_20}
\end{figure}


De manière générale, l'économie mauricienne gagne en vigueur, en grande partie grâce aux exportations de services, dont le tourisme notamment. Parallèlement, les principaux moteurs de croissance sectorielle devraient continuer à bien se porter : les services financiers, la transformation des aliments, la vente au détail et en gros, ainsi que le secteur des technologies de l'information et des communications. En outre, l'économie mauricienne profite de sa récente diversification dans des domaines à plus forte valeur ajoutée, tels que le tourisme médical et l'enseignement supérieur\footnote{\url{https://www.afdb.org/en/countries/southern-africa/mauritius/mauritius-economic-outlook}} \footnote{Macroeconomic performance : \url{https://www.afdb.org/en/countries/southern-africa/mauritius/mauritius-economic-outlook}}.

\vspace{-5mm}
\subsection{Épargne, flux de capitaux et investissements directs étrangers}
\label{subsec:epargne_flux}
Quand on observe la \autoref{wrap-fig:21}, on constate que beaucoup de nouveaux capitaux arrivent sur l'île Maurice. 

\begin{wrapfigure}{r}{0.6\textwidth}
		\includegraphics[width=0.57\textwidth]{images/2/21}
		\caption[IED et ressources financières externes]{IED et ressources financières externes : \url{http://uis.unesco.org/apps/visualisations/research-and-development-spending/}}
		\vspace{-5mm}
		\label{wrap-fig:21}
\end{wrapfigure} 
L'île paradisiaque est désormais une terre d'investissement pour les grands capitaux internationaux. 
D'ailleurs, lorsqu'on compare 2005 avec 2017, le changement est frappant : en 2005, les flux financiers sortants dépassaient les flux financiers entrants, alors qu'en 2017, il y a près de cinq fois plus de flux financiers entrants que de flux financiers sortants.

Le déficit budgétaire s'est légèrement creusé, passant de $3,4\%$ du PIB en 2017 à environ $3,5\%$ en 2018, mais devrait revenir à $3,4\%$ en 2019 en raison de l'assainissement budgétaire et du versement en cours d'un don de l'Inde. La viabilité de la dette publique est considérée comme globalement positive, même si un assainissement budgétaire serait nécessaire pour que le pays puisse atteindre l'objectif de dette publique statutaire récemment ajusté, à savoir $60\%$ du PIB d'ici à décembre 2021.

La politique monétaire a été accommodante compte tenu de la faible inflation et de la nécessité de soutenir l'activité intérieure. L'inflation est passée de $3,7\%$ en 2017 à environ $5,1\%$ en 2018, en raison principalement de pénuries de production alimentaire résultant de pertes causées par de fortes précipitations. Le déficit du compte courant s'est creusé, passant de $6,6\%$ du PIB en 2017 à environ $8,8\%$ en 2018, selon les estimations. Les réserves brutes internationales se sont élevées à 11 mois d'importations. Les principales exportations sont les vêtements, la canne à sucre, le poisson transformé et les fleurs coupées. Les exportations de services continuent également d'augmenter, tirées par le tourisme et les services financiers.

%\begin{wrapfigure}{l}{0.68\textwidth} 
%		\includegraphics[width=0.65\textwidth]{images/2/22}
%		\caption[Balance commerciale]{Balance commerciale : \url{https://unctadstat.unctad.org/CountryProfile/GeneralProfile/fr-FR/480/}}
%		\vspace{-5mm}
%		\label{wrap-fig:22}
%\end{wrapfigure} 
Les perspectives économiques sont propices en raison de conditions extérieures favorables et de la hausse des investissements publics. La croissance du PIB réel devrait
atteindre $4,0\%$ en 2019 et $3,9\%$ en 2020. La croissance pourrait même s'accélérer si le programme d'infrastructure publique du gouvernement s'accélère et stimule l'investissement privé. Le déficit de la balance courante devrait rester élevé, à $8,2\%$ du PIB en 2019, en raison de la hausse des prix des produits de base et des importations importantes pour le programme d'infrastructure.

L'augmentation des prix mondiaux de l'énergie et des produits alimentaires devrait entraîner des pressions inflationnistes et une contrainte sur la position extérieure de l'économie insulaire. Un ralentissement économique des principaux partenaires commerciaux européens (en raison des tensions commerciales mondiales ou du Brexit) pourrait entraver le tourisme ainsi que les exportations de biens. Parmi les autres obstacles possibles à la croissance, citons la base restreinte de compétences nationales et les risques naturels liés au changement climatique.


\subsection{Institutions, crédibilité et normes sociales}\label{subsec:institutions_credibilite}

Le pays est en train de devenir rapidement une plaque tournante pour le commerce, la réexportation, la logistique et la distribution, devenant ainsi un point de départ pour les entreprises locales et internationales à la recherche d'opportunités sur le continent (voir \autoref{wrap-fig:24} pour son niveau de compétitivité). 
Maurice devient également une plateforme financière ou une passerelle vers l'Afrique.
En 2016, les banques et les compagnies d'assurance établies à Maurice ont injecté plus de 50 millions de dollars dans l'économie du Kenya par le biais d'acquisitions et d'investissements. 

L'expertise mauricienne réhabilite et gère également les industries sucrières en Côte d'Ivoire, à Madagascar, au Mozambique, en Tanzanie et en Ouganda.
\newpage

\begin{wrapfigure}{r}{0.75\textwidth}
		\includegraphics[width=0.72\textwidth]{images/2/23}
		\caption[Réformes du contexte d'affaires]{Réformes du contexte d'affaires extrait de l'African Economic Outlook 2017}
		\vspace{-5mm}
		\label{wrap-fig:23}
\end{wrapfigure} 


Élément central de notre recommandation d'investissement futur, le contexte entrepreneurial favorable de l'île Maurice qui s'inscrit dans la tendance africaine (voir \autoref{wrap-fig:23}). Pratiquement tous les indicateurs clés de l'étude Doing Business (\autoref{fig:25_26_27_28_29_30}) ont été réformés. En effet, au cours des dix dernières années, Maurice a réformé plus d'une fois presque tous les domaines évalués. 


\begin{wrapfigure}{r}{0.75\textwidth}
		\includegraphics[width=0.72\textwidth]{images/2/24}
		\caption{Amélioration de la compétitivité mondiale des pays africains}
		\vspace{-5mm}
		\label{wrap-fig:24}
\end{wrapfigure} 
Par exemple, sept réformes plus tard depuis 2005, c'est maintenant douze fois plus rapide d'enregistrer une entreprise, notamment grâce à l'informatisation du processus (moins de 0,5 jour en ligne). Même principe en matière d'incorporation alors que des réformes ont permis d'accélérer de 10 fois le temps nécessaire à l'incorporation. Du côté de la distribution d'électricité, les pays du top 20 jouissent d'outils automatisés permettant d'assurer un rétablissement rapide en cas de panne. Dans les secteurs de la construction et de l'administration des terrains, des normes plus strictes, notamment en matière d'inspection obligatoire, contribuent à tirer vers le haut la cote de l'île Maurice. Enfin, les indicateurs inhérents aux infrastructures juridiques et administratives sont également tous au vert, notamment en ce qui concerne l'efficacité et la transparence. Le cadre légal relatif à l'insolvabilité des entreprises est solide et sérieux et l'automatisation de plusieurs processus (paiement des taxes en ligne, processus d'exportation accéléré, etc.) contribue à créer un contexte économique favorable.


\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{\textwidth}
                        \includegraphics[width=\textwidth]{images/2/25}
                        \caption{Introduction ou amélioration des systèmes électroniques de paiement des impôts}
                        \label{fig:26}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                        \includegraphics[width=\textwidth]{images/2/26}
                        \caption{Amélioration des processus de vérification fiscale et correction des processus d'impôt sur le revenu des sociétés}
                        \label{fig:27}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                        \includegraphics[width=\textwidth]{images/2/27}
                        \caption{Amélioration du processus de remboursement de la TVA}
                        \label{fig:28}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                        \includegraphics[width=\textwidth]{images/2/28}
                        \caption{Amélioration de l'administration douanière et des inspections à l'exportation et à l'importation}\
                        \label{fig:29}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                        \includegraphics[width=\textwidth]{images/2/29}
                        \caption{Facilité de faire des affaires}
                        \label{fig:30}
        \end{subfigure}%
        %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
        \caption[Facilité de faire des affaires et réformes]{Facilité de faire des affaires et réformes : \url{https://www.doingbusiness.org/content/dam/doingBusiness/media/Annual-Reports/English/DB2019-report\_web-version.pdf}}
		\vspace{-5mm}
        \label{fig:25_26_27_28_29_30}
\end{figure}

Maurice a l'une des économies les plus prospères et compétitives en Afrique. En 2014, elle est classée premier pays du continent pour ce qui est du climat d'investissement, de la bonne gouvernance pour la cinquième année consécutive et 8$^\text{e}$ dans le monde en termes de liberté économique.

\subsection{Politique et aspects socioculturels pouvant impacter la croissance}
\label{subsec:politique_social}

La politique étrangère de Maurice est basée sur une relation de confiance avec les autres pays. Elle est membre de l'Organisation Mondiale du Commerce (\href{https://www.wto.org/indexfr.htm}{OMC}), du \href{https://fr.wikipedia.org/wiki/Commonwealth}{Commonwealth}, de la \href{https://fr.wikipedia.org/wiki/Francophonie}{Francophonie}, de l'\href{https://fr.wikipedia.org/wiki/Union_africaine}{Union africaine}, de la \href{https://www.commissionoceanindien.org}{Commission de l'océan Indien}, de la \href{https://www.sadc.int}{Southern African Development Community}, du Marché commun de l'Afrique orientale et australe (\href{https://fr.wikipedia.org/wiki/Marché\_commun\_de\_l\%27Afrique\_orientale\_et\_australe}{COMESA}) et de l'Indian Ocean Rim Association for Regional
Cooperation (\href{https://www.iora.int/en}{IOR-ARC})\footnote{Source : \url{https://fr.wikipedia.org/wiki/Politique\_\%C3\%A9trang\%C3\%A8re\_de\_Maurice}}.

Le système fiscal mauricien est connu pour être l'un des plus légers au monde. Maurice est en effet considérée comme un paradis fiscal et a signé avec de nombreux pays africains des conventions de non double imposition. Des failles dans ces conventions permettent à des multinationales installées à Maurice de ne pas payer d'impôts. Maurice compte plus de 20 000 sociétés offshores. Grâce à ce système fiscal, de nombreuses grandes firmes (assureurs, banques) décident d'ouvrir des filiales à Maurice. Cela entraîne une hausse du nombre d'emplois et par la même occasion du PIB.

Cette ascension phénoménale donne maintenant un nouveau surnom au pays : \og Maurice, la miraculeuse \fg{}. De quoi défier les prédictions initiales de James Meade, économiste gagnant d'un prix Nobel, qui en 1961 avait écrit : "It is going to be a great achievement if [the country] can find productive employment for its population without a serious reduction in the existing standard of living\ldots{} [The] outlook for peaceful development is weak." Bien que plusieurs indicateurs économiques confirment la vitalité de l'île Maurice, des écueils majeurs entrent en jeu lorsqu'il est question des habitudes de consommation d'alcool des Mauriciens.

\begin{wrapfigure}{r}{0.73\textwidth}
		\includegraphics[width=0.7\textwidth]{images/2/30}
		\caption[Consommation d'alcool à Maurice]{Consommation d'alcool à Maurice : \url{https://www.who.int/substance_abuse/publications/global_alcohol_report/profiles/mus.pdf?ua=1}}
		\vspace{-5mm}
		\label{wrap-fig:30}
\end{wrapfigure} 

Tout d'abord, il faut comprendre que la segmentation religieuse de la population impactera invariablement la consommation d'alcool : hindoues $48\%$, catholiques romains $23,6\%$, musulmans $16,6\%$, autres chrétiens $8,6\%$, autres $2,5\%$. C'est donc dire que $64,6\%$ de la population, de par sa religion (Hindouisme et Islam), consomme peu ou pas d'alcool, ce qui constitue un frein naturel à la croissance de la demande locale en matière d'alcool. Qui plus est, des données récentes illustrent que les Mauriciens ne sont pas de grands consommateurs d'alcool alors qu'ils consomment en moyenne 2,5L d'alcool par année et que de ces 2,5L, $54\%$ s'avèrent être de la bière, comme l'illustre la \autoref{wrap-fig:30}. Ainsi, les Mauriciens consomment en moyenne 27 bières/année ($\frac{2,5L}{5\%} \cdot 54\%$).

\newpage

\begin{wrapfigure}{l}{0.75\textwidth}
		\includegraphics[width=0.72\textwidth]{images/2/31}
		\caption{Évolution de l'inflation -- 2007--2019}
		\vspace{-5mm}
		\label{wrap-fig:31}
\end{wrapfigure} 
Or, les mœurs changent, de plus en plus de Mauriciens consomment de l'alcool sous l'influence des courants internationaux, notamment des habitudes européennes et sud-africaines. Qui plus est, cette faible consommation historique fait en sorte que bien peu de joueurs sont actuellement implantés sur le marché de l'île Maurice, un brasseur d'envergure internationale pourrait facilement s'y implanter et accaparer une part importante du marché existant tout en s'assurant de mettre les bases d'une présence pour les années de croissance à venir. 
Avec un taux sous les 2\% en depuis le début~2019, l'inflation a été globalement modérée (voir \autoref{wrap-fig:31}).

\end{document}