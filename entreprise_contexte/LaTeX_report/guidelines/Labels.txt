Naming convention for labels:
Figures:
	\label{fig:"name"}

Equations:
	\label{eq:"name"}
	
Sections:
	\label{sec:"name"}

Subsections:
	\label{subsec:"name"}

Subsubsections:
	\label{subsubsec:"name"}


Chapter: 	\label{chap:"name"}

table: 	\label{tab:"name"}
