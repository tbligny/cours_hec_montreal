\documentclass[Rapport.tex]{subfiles}
\begin{document}
\chapter[Question 1]{Question 1 : Quels sont les facteurs clés de succès dans l’industrie du gaz de schiste ?}
\label{chap:question1}
\vspace{-40pt}

L'exploration, l'exploitation et le commerce du gaz de schiste sont des
activités coûteuses, règlementées, et où les choix de localisation sont
limités. La nature même de cette industrie la rend également sujette à
controverses. Au travers du diamant de Porter \citep[\textit{The Competitive Advantage of Nations}]{bib-porter}, nous nous intéresserons
ici aux facteurs qui, conjointement, forment les clés du succès dans ce
milieu sensible. Dans \citep{bib-porter}, Porter fait état des quatre attributs qui expliquent pourquoi
une entreprise au sein d'une nation particulière est compétitive au
niveau international.

\paragraph{La stratégie, la structure et la rivalité des sociétés}
L'industrie du gaz de schiste a pour vocation d'étendre la capacité
mondiale de production de gaz et de pétrole en incluant des méthodes non
conventionnelles d'extraction de ces matières. C'est une industrie très
concurrentielle (cf. cas, \citep[Page 4]{bib-cmooc}) car de gros acteurs internationaux s'y
développent avec de forts investissements et une longue expertise, mais de petits acteurs régionaux supportés par les gouvernements
locaux sont également présents. Si les facteurs géographiques de localisation des réserves
limitent le nombre d'acteurs capables d'intervenir dans ce secteur, la
rivalité s'en trouve exacerbée quant au partage des sols exploitables et
de la main-d'œuvre. Il s'agit en effet d'opérer dans des zones
généralement reculées et isolées des grands centres urbains tandis que
le besoin de main-d'œuvre est élevé. Dans ces conditions, la concurrence
est forte pour attirer du personnel, ce qui se reflète dans les salaires
pratiqués comme en témoigne la \autoref{fig:3} en Alberta. Les entreprises
du secteur procèdent effectivement à la mise aux enchères de leur
politique salariale. Ces surcoûts liés à la distance et au coût du
travail se doivent d'être compensés par une optimisation des coûts
d'exploitation, un haut degré d'automatisation, de voies de transport
des matières premières efficientes et des réserves pérennes. L'avantage
revient alors aux sociétés détentrices de la meilleure technologie dont
les économies liées lui permettent plus d'agressivité sur l'attractivité
de la main-d'œuvre. Quant à la pérennité de l'activité, les permis
fonciers d'exploitation constituent un avantage concurrentiel notable sur le long terme.

Par ailleurs, les groupes activistes opposés à la destruction de
l'environnement à des fins mercantiles, tel que Greenpeace notamment \citep{bib-green},
représentent une barrière pour l'industrie. Bien que la sécurité
environnementale de la production de gaz de schiste soit encore à
l'étude, de nombreux exploitants se heurtent à une
forte opposition de la part des groupes environnementaux en raison de
préoccupations en matière de santé et de sécurité liées à la technologie
de fracturation hydraulique et à l'utilisation d'eau.

\paragraph{Les états de la demande}
Les prix du gaz naturel sont actuellement bas dans certaines
régions \citep{bib-forbes}, et la richesse des nouvelles zones de
gaz de schiste viables pourrait faire chuter encore plus les prix.
Toutefois, à mesure que la production de pétrole et de gaz à partir de
sources classiques continue de diminuer, le prix local du gaz naturel
par rapport aux autres sources d'énergie déterminera si les
investissements à long terme nécessaires à la mise en valeur et à
l'exploitation d'une zone produiront des rendements suffisants.

Pour de nombreux pays qui dépendent des importations de gaz naturel, la
sécurité énergétique est une préoccupation. Le gaz de schiste pourrait
les aider à devenir plus autosuffisants. D'autre part, les pays
exportateurs traditionnels de pétrole et de gaz devront réagir à
l'évolution de leurs marchés. Les questions politiques qui en
résulteraient pourraient modifier radicalement les relations entre les
pays.

Il est à noter que la demande pour le gaz de schiste aux États-Unis devrait augmenter au cours des prochaines années \citep{bib-eiaG}, ce qui constitue une opportunité de marché important.

\paragraph{Les industries support liées}
La principale industrie support liée est celle de l'infrastructure de distribution et de transport du gaz de schiste. Pour rencontrer le succès, il ne suffit pas d'avoir des réserves. Les sites de production doivent être adéquatement desservis par des routes et des pipelines, par exemple, et
des installations spéciales de traitement et de transport sont nécessaires pour liquéfier le gaz naturel destiné au transport maritime.

\vspace{-5mm}
\paragraph{Les états des facteurs}
\begin{itemize}
\item La géographie joue un rôle particulièrement important lorsqu'il s'agit
de produire du gaz de schiste. En effet, cette industrie n'a de sens
qu'en s'installant dans des zones où les réserves en gaz sont abondantes
et recouvrables à un prix économiquement viable. Sans ces deux facteurs,
l'investissement initial en infrastructure risquerait de ne pas être
rentabilisé, faute de ressources à exploiter ou de coût d'exploitation
trop élevé. La \autoref{fig:2} montre la position géographique de
l'estimation des 48 bassins majeurs de gaz dans le monde. On constate à
ce propos que l'Alberta possède un des plus gros bassins au monde, ce
que nous garderons à l'esprit dans la suite du devoir. Les zones de gaz
de schiste doivent être suffisamment importantes pour justifier
l'investissement coûteux nécessaire à son
extraction et son exploitation. La zone doit donc être à proximité des marchés pour faciliter la distribution.

\item Exploiter le gaz de schiste n'a été rendu possible qu'au travers
d'innovations technologiques majeures qui nécessitent une main-d'œuvre
qualifiée abondante. Cependant, l'industrie fait en réalité appel à une
majorité de travailleurs semi-spécialisés et peu qualifiés pour les
activités de pompage et de transport (camionnage, etc.). L'enjeu est
alors d'attirer des dizaines voire des centaines de milliers d'individu (cf. \autoref{fig:1})
à venir travailler dans des environnements urbains qui n'existent
quasiment que pour l'industrie pétrolière, dans des conditions de
travail intenses.

\item Une forte capacité d'investissement est nécessaire pour pouvoir
démarrer une activité dans le gaz de schiste. Cela ne s'adresse donc qu'à des entreprises historiques, des États ou des sociétés supportées par de riches investisseurs.
\end{itemize}

À ces quatre attributs principaux s'ajoutent les attributs
circonstanciels ci-après :
\vspace{-5mm}
\paragraph{La chance}
Le pétrole est une commodité dont le prix d'échange est dicté par les
marchés financiers mondiaux. Or l'industrie du gaz de schiste a des
coûts opérationnels plus élevés que l'industrie pétrolière
traditionnelle\footnote{cf. cas \citep[Page 4]{bib-cmooc} : 40-90\$/baril pour le seuil de
rentabilité du gaz de schiste vs. 20 CAD/baril au Moyen-Orient et Afrique}. Elle n'est donc
viable que lorsque le prix du baril est suffisamment haut pour justifier
ces dépenses plus importantes. Le facteur chance joue donc un rôle
primordial quant à la viabilité d'un projet énergétique dont la
rentabilité dépend d'un prix mondial unique, lui-même régi par les
tensions géopolitiques et la santé de l'économie mondiale.
\paragraph{Le gouvernement}
Les entreprises privées ont besoin de l'appui de leur pays pour
développer une capacité de production de gaz de schiste à grande
échelle. Un régime de réglementation stable –- où les lois ne divergent pas trop d'une province à l'autre -- et bien développé, un accès prévisible aux permis et aux
licences et des subventions gouvernementales pour l'exploration et le
développement sont essentiels. L'enjeu dans les \og démocraties
occidentales \fg{} est qu'un des objectifs du gouvernement est de se
maintenir au pouvoir grâce au support de sa population. Le gouvernement
a donc naturellement des d'affinités avec les considérations de ses
citoyens et doit sans cesse procéder à un arbitrage entre génération
d'emplois et de devises d'un côté, et prise en compte des préoccupations
des particuliers de l'autre.






%Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis metus dapibus libero dictum, et semper nibh vehicula. Fusce eget eros lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse accumsan nisi eget augue mollis, quis gravida eros maximus. Sed ultricies neque leo, a elementum sapien euismod at. Donec ullamcorper, libero vel laoreet congue, metus ante viverra dolor, vehicula vestibulum mauris ipsum eu nibh. Morbi bibendum velit sed erat convallis tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec lacinia pharetra dolor ac aliquam.
%
%Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis pretium eros non dui malesuada elementum. Sed malesuada nibh ut purus finibus condimentum. Phasellus eleifend nisi eros, ac consectetur velit bibendum ac. Nunc sit amet tempus risus. Sed lacus risus, placerat fringilla ipsum a, feugiat gravida justo. Morbi interdum diam ut nunc laoreet, non aliquet enim auctor. Cras lacinia, risus a faucibus gravida, massa arcu mattis massa, euismod rutrum erat justo eu dolor. Duis ac risus tincidunt lacus semper semper et in purus. Suspendisse potenti. Nullam cursus sollicitudin efficitur.
%
%Aliquam non nibh sed enim cursus rhoncus. Integer dignissim nisi sit amet magna vestibulum, in aliquam elit ultrices. Donec pulvinar elit quis erat porttitor, a semper purus rutrum. Phasellus quis erat odio. Fusce sodales vestibulum commodo. Morbi porttitor dui id accumsan commodo. Mauris quis nisl in quam placerat mollis eget in sem. Nam auctor vulputate mauris. Nunc malesuada ex at mauris auctor tempor. Cras sit amet tincidunt nisi. Quisque malesuada libero in egestas volutpat. Integer nibh ligula, ultrices in ullamcorper in, interdum in mi. Donec id augue quis massa vulputate lacinia. Nullam dapibus pulvinar nulla. Sed rutrum nisi id lorem mattis, ac efficitur enim suscipit.
%
%Morbi ut pharetra tellus. Nunc lobortis leo eu elit fringilla, id ultrices enim efficitur. Maecenas vulputate pretium lorem, sed placerat velit tempor ac. Praesent quis erat at orci mollis mollis condimentum id nulla. Praesent lobortis et nibh ut feugiat. Nullam sit amet rhoncus ante. Duis et neque lobortis, gravida odio vitae, tincidunt urna. Fusce mattis pharetra felis, non laoreet ipsum lobortis a. Sed at nulla porttitor, molestie diam eu, tincidunt leo. Phasellus eu justo ac nunc efficitur eleifend eu nec sem. Nulla pretium bibendum neque, vel interdum odio pulvinar sed. Aenean nec interdum odio, eu laoreet nunc. Donec congue lectus eu arcu ultricies viverra. Maecenas neque dui, faucibus sit amet augue eget, faucibus ullamcorper leo.
%
%Vivamus vulputate, justo auctor molestie consectetur, tellus massa facilisis felis, vitae tempor ipsum ipsum nec massa. Vivamus eu consequat est, id euismod eros. Morbi at congue magna. Fusce blandit a justo quis ullamcorper. Donec ante mi, euismod eu sagittis a, tincidunt quis sapien. Aliquam erat volutpat. Ut venenatis nisi erat, nec commodo ipsum pellentesque in. Fusce ex ex, imperdiet facilisis metus ut, euismod sodales felis. Vivamus faucibus congue velit vitae accumsan. Maecenas pharetra placerat mauris eu laoreet. Fusce placerat urna nibh, non facilisis ligula luctus eu.
%
%Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut dui non urna dignissim tempor. Nam rhoncus, ex ut rutrum efficitur, lacus eros bibendum ex, non luctus nibh orci ac diam. Duis pretium turpis nec lacus pellentesque, sed faucibus lectus dignissim. In rutrum tristique turpis, ut posuere nulla suscipit sed. Sed ut dignissim elit, quis porttitor nunc. Mauris porttitor mollis interdum. Suspendisse eu eros vitae nibh tristique consectetur ut quis eros. Fusce ac felis tincidunt nibh pulvinar accumsan varius sit amet nibh. Morbi eget scelerisque risus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
%
%Nulla id arcu vulputate, pulvinar sem id, pulvinar sem. Fusce finibus, felis nec posuere tincidunt, mauris nisl maximus nunc, nec eleifend sem mauris eu ipsum. Maecenas odio purus, pellentesque non nulla quis, volutpat porta libero. Aenean vehicula dui magna, non laoreet turpis ornare eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultricies enim quis porta consectetur. Nulla risus erat, mollis eget lobortis et, consectetur id lacus. Mauris posuere consectetur augue. Suspendisse viverra nibh fermentum sapien venenatis condimentum ut eget ligula.
%
%Nam tempus risus ac erat ultrices, quis scelerisque turpis euismod. Nam eget maximus risus, sit amet vehicula nunc. Morbi suscipit turpis non ligula iaculis, vitae consequat mauris tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed sollicitudin mauris non semper fermentum. Proin mattis tellus sit amet enim congue sodales. Sed in aliquam tortor, a molestie nulla. Curabitur malesuada consequat eros, quis malesuada purus eleifend a.
%
%Maecenas nec pulvinar est. Praesent est nulla, auctor ultrices gravida vitae, bibendum in enim. Integer in quam posuere, cursus nisi vitae, interdum ipsum. Fusce nunc felis, vehicula eu blandit in, tincidunt quis ex. Vivamus vel viverra ligula. Nulla quis dolor in nunc iaculis varius eu volutpat ante. Sed at massa id sem eleifend suscipit. Pellentesque luctus purus lectus, eu consequat nulla semper at. Nam vulputate diam porta, iaculis neque sit amet, tristique elit. Suspendisse id eros molestie, efficitur elit et, sollicitudin metus. Cras semper feugiat felis in consequat. Duis efficitur eros a augue convallis, et fermentum odio efficitur.
%
%Sed maximus turpis quis ex eleifend elementum. Quisque vitae purus aliquam, molestie ligula at, semper eros. Etiam vel tincidunt mi. Fusce quis vulputate ligula. Integer posuere iaculis tincidunt. Quisque congue fringilla enim sed dictum. Aliquam at ornare nisl. Nunc lacus orci, sagittis vitae elit nec, mollis iaculis tortor. Suspendisse volutpat felis ex, ut accumsan nisl luctus non. Nam sed enim ipsum. Quisque volutpat metus nibh, sit amet varius magna faucibus nec. Aliquam ac massa et mi commodo ullamcorper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
%
%Sed mattis ullamcorper ipsum, nec suscipit urna pharetra at. Integer porttitor iaculis enim, non iaculis tortor finibus in. Maecenas ligula quam, auctor et nisl ut, auctor tempor nisl. Proin finibus metus sed ultricies auctor. Quisque ac nisi semper, finibus massa in, viverra nisi. Pellentesque pulvinar ligula a est consectetur mattis. Aliquam finibus viverra urna ullamcorper rhoncus. Aenean placerat nibh lorem, sed elementum metus bibendum vitae.
%
%Nullam vulputate elit non ante posuere, sed laoreet orci pulvinar. Cras ac mi blandit, pulvinar risus ut, venenatis dui. Vestibulum id pretium magna. Sed euismod congue leo. Duis non diam id lacus ullamcorper ornare. Donec condimentum dignissim odio sit amet cursus. Suspendisse sed semper ex, et dignissim magna. Proin ac metus sed mauris consequat dictum. Nulla dignissim ipsum quis feugiat posuere. Nullam scelerisque nec ex in bibendum. Nam tincidunt molestie sapien nec pellentesque. Phasellus rutrum porta convallis. Praesent pulvinar nulla arcu, eget cursus urna cursus ac. Aenean lobortis, diam ut pharetra vulputate, ex quam cursus leo, quis blandit felis nisi sit amet erat. Aenean sagittis ex a eros sagittis porttitor.
%
%Proin tortor neque, tristique tincidunt nisl ac, pellentesque hendrerit justo. Duis condimentum, turpis vitae tempor volutpat, mauris nunc convallis tellus, laoreet tempor magna leo quis nibh. Nulla facilisi. Ut iaculis ante vitae ipsum aliquet, ut faucibus magna pretium. Morbi sodales, purus posuere molestie placerat, eros odio egestas tellus, ut tempus lacus dolor sed justo. Quisque sodales, eros quis lacinia condimentum, sem velit bibendum ipsum, ut mattis ante augue vel diam. Sed lacinia porta lectus, a semper mi dictum sit amet. Sed et pharetra lectus. Aenean dignissim nunc eget libero lacinia mattis. Praesent eu sodales ex, vitae sagittis dolor. Cras egestas suscipit eros, a interdum sapien hendrerit sed. Quisque placerat eu turpis ac tempus. In ac elit sollicitudin, vehicula enim id, tincidunt augue. Vivamus facilisis, eros quis lacinia auctor, nunc diam molestie mauris, in dictum velit tortor eu magna. Nullam congue, magna condimentum congue sollicitudin, urna elit aliquet magna, nec condimentum metus magna at enim. Mauris arcu sem, iaculis vel turpis ut, lobortis fringilla lectus.
%
%Nam sit amet finibus justo. Integer ac ultrices dui. In dictum metus in maximus porta. Curabitur convallis sem orci, a egestas nibh convallis in. Vivamus quis arcu imperdiet, porttitor odio non, laoreet risus. Vivamus semper nulla quis varius vestibulum. Quisque congue eget diam non efficitur. Aliquam sed nibh dignissim, semper est pretium, luctus metus. Ut suscipit dui ac massa eleifend, sit amet aliquam ex laoreet. Aliquam vel velit id purus condimentum dignissim sit amet id mi. Curabitur elit massa, tincidunt eu tempus aliquam, hendrerit sed lacus. Pellentesque eu varius tellus, sed lacinia dolor. Quisque molestie urna a ligula molestie dignissim.
%
%Quisque convallis elit vel elementum semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur eget sollicitudin ante. Integer fermentum congue ornare. Sed sit amet neque augue. Etiam facilisis suscipit nisi, et blandit risus dictum nec. Pellentesque fermentum euismod nunc vitae ullamcorper. Duis mi ipsum, sodales eget tortor eleifend, suscipit porta neque. Sed posuere blandit purus pellentesque consequat. Quisque tincidunt convallis quam, vel consectetur augue luctus eu. Nullam finibus dictum enim id malesuada. Nam maximus pellentesque justo, vel imperdiet mi feugiat in. Duis posuere tellus eget laoreet pharetra. Phasellus ornare enim tellus, quis vulputate leo sollicitudin a. Proin consectetur lacus nisl, eget convallis risus placerat id.
%
%Nulla id tellus eget eros hendrerit placerat sed id est. Vestibulum eu elit non massa placerat luctus vitae ac arcu. Nullam id orci quam. Donec blandit mi neque, iaculis euismod dolor imperdiet nec. Donec mi nulla, rutrum porta dui nec, efficitur dignissim neque. Sed et condimentum lacus. Phasellus condimentum tortor non lacus pellentesque, in varius nulla condimentum. Aliquam nisi augue, laoreet quis rhoncus ut, vestibulum sit amet est. Proin posuere, arcu et sodales ultrices, justo risus fringilla odio, sed interdum eros sapien quis mauris.
%
%In tristique nec felis id sagittis. Donec magna libero, pellentesque nec nibh sed, eleifend laoreet ipsum. In vitae nisi tincidunt, aliquam odio ac, vestibulum ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed auctor, metus non egestas congue, massa nisl porta elit, et varius nulla urna at mauris. Duis pharetra neque eget sem hendrerit congue. Suspendisse vel aliquam dolor. Quisque accumsan volutpat tellus eget dictum. In hac habitasse platea dictumst. Mauris consequat et quam quis laoreet. Duis cursus faucibus urna vel pharetra. Fusce feugiat turpis non nisi blandit, ut mollis mi suscipit. Vivamus sodales lacus et sodales tempor.
%
%Etiam mattis, ligula et interdum tempus, mi justo porttitor sem, non blandit augue quam quis mauris. Nullam mattis velit ac eros pretium, nec dictum tellus tincidunt. Vestibulum mattis viverra porta. Fusce sagittis tempus eleifend. Morbi semper tellus vel sapien ultrices placerat. Suspendisse suscipit id risus ornare laoreet. Nulla non sapien eget augue ultricies sollicitudin. Nulla quis scelerisque tortor. Praesent vitae ullamcorper magna. Etiam urna enim, aliquam vel finibus ultricies, luctus consequat leo. Nulla finibus urna id sem lacinia, eget auctor risus elementum. In pellentesque ornare vehicula. Phasellus eget leo vel justo volutpat feugiat.
%
%Duis blandit rhoncus arcu ut iaculis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent vitae leo consequat, elementum arcu quis, lobortis est. Ut ullamcorper ex quis pulvinar ultricies. Donec commodo viverra sapien, quis scelerisque odio mollis nec. Pellentesque vehicula hendrerit turpis in tempus. Pellentesque nec ex porttitor diam pulvinar bibendum a vel mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In iaculis aliquam tempus. Nullam eu volutpat odio. Nunc pretium lacus sit amet porttitor elementum. Etiam tincidunt augue imperdiet libero tempus, sit amet tempus libero faucibus. Integer pulvinar vestibulum ante sed semper. Aenean velit magna, ultricies et vehicula eget, ultrices sit amet justo.
%
%Vivamus a metus nisl. Cras vel dolor eu felis blandit aliquam. Vestibulum nec urna tristique, imperdiet urna vitae, sollicitudin lorem. Nam faucibus lobortis semper. Curabitur eu velit ac enim dapibus rhoncus et nec mi. Phasellus vel volutpat magna. Donec a augue sem.
%
%Aliquam vulputate risus ut rhoncus dictum. Ut varius et ligula vel ornare. Cras in arcu imperdiet, finibus felis at, convallis ligula. Pellentesque pretium diam tortor, a pharetra urna luctus vitae. Aliquam et ipsum convallis, posuere ante et, gravida enim. In turpis ipsum, mollis et convallis at, dapibus elementum ligula. Integer eros elit, accumsan non vestibulum sed.

















%\lettrine{C}e rapport présente les résultats de notre étude macroéconomique sur la possibilité d'étendre les activités de \textsc{BeerHaven Co.} outre-mer. Le pays visé par cette étude est la \textbf{République de Maurice (\og Maurice \fg{})}. Un investissement de 20 millions d'USD est prévu afin de démarrer des opérations brassicoles et de distribuer le produit au niveau national. \textsc{BeerHaven Co.} a retenu nos services afin de préparer ladite étude.
%
%Le rapport a été préparé à l'intention de \textsc{BeerHaven Co.} Ses principaux objectifs sont les suivants :
%\begin{itemize}
%\item  Déterminer le potentiel de marché de Maurice
%\item  Déterminer les hypothèses de croissance de ce marché au cours des prochaines années
%\item  Identifier les risques macroéconomiques liés à cet investissement
%\item  Émettre une recommandation et une opinion
%\end{itemize}
%
%\section{Présentation du pays}\label{sec:pres_pays}
%
%\begin{wrapfigure}{r}{0.6\textwidth}
%		\includegraphics[width=0.57\textwidth]{images/1/1}
%		\caption{Emplacement de Maurice dans le monde}
%		\vspace{-5mm}
%		\label{wrap-fig:1}
%\end{wrapfigure} 
%Maurice est une ancienne colonie française et anglaise. L'île a obtenu son indépendance le 12 mars 1968\footnote{\url{https://fr.wikipedia.org/wiki/Portail:Maurice}}. Elle est située dans l'\href{https://fr.wikipedia.org/wiki/Archipel}{archipel} des \href{https://fr.wikipedia.org/wiki/Mascareignes}{Mascareignes} à environ neuf cents km à l'est de \href{https://fr.wikipedia.org/wiki/Madagascar}{Madagascar} (voir \autoref{wrap-fig:1}). Sa superficie totale est de $1865$ km$^2$. Maurice fut un lieu de passage important sur la route des Indes grâce à son port naturel -- Port-Louis -- devenu la capitale. C'est ainsi que Maurice fut nommée \og l'étoile et la clé de l'océan indien \fg{}. Située près du tropique du Capricorne, Maurice a un climat tropical. L'île Maurice possède également plusieurs territoires, à savoir Rodrigues, les îles Agaléga, l'archipel de Saint-Brandon et l'archipel des Chagos. Néanmoins, comme la population y est très faible sur ces territoires, notre analyse ne portera que sur Maurice.  Le pays est composé de différentes ethnies : $68\%$ de la population est d'origine indienne, $27\%$ métisse, $3\%$ chinoise et $2\%$ européenne\footnote{\url{http://www.axl.cefan.ulaval.ca/afrique/maurice.htm}}. Les langues les plus parlées dans la vie quotidienne sont le créole ($77,3\%$), le bhodjpouri ($5,2\%$), le français ($4,0\%$) et l'hindi ($2,8\%$), ce qui représente $89,3\%$ des locuteurs. La monnaie officielle est la roupie mauricienne\footnote{\url{https://www.ile-maurice.fr/infos-pratiques/}}.
%
%\section{Contexte économique}\label{sec:context_eco}
%
%À la suite de son indépendance du Royaume-Uni en 1968, la pierre angulaire économique pour Maurice était l'agriculture, en particulier l'industrie du sucre.
%
%Dès lors, Maurice a commencé à diversifier son économie abandonnant une politique de substitution des importations qui n'avait pas été fructueuse à cause du faible marché interne. Le pays a décidé d'accentuer ses exportations à l'aide d'une politique pour la mise en place de zones d'activité économique permettant, selon une analyse de la Banque mondiale, d'accélérer le processus d'approbation des fabricants orientés vers les marchés extérieurs. Concurremment, à travers son expansion, le pays s'est également doté de politiques économiques fortes en s'engageant entre autres dans un processus d'amélioration de ses indicateurs économiques, notamment en matière de développement du capital humain. Ces indicateurs seront abordés plus en détail dans le \autoref{chap:hypotheses_croissance}.
%
%\section{Potentiel de marché}\label{sec:potentiel_marche}
%
%\subsection{Évolution du contexte économique}\label{subsec:evol_contexte_eco}
%
%D'emblée, il est difficile d'identifier un marché africain plus florissant que celui de l'île Maurice. En effet, une transformation impressionnante au cours des trois dernières décennies a permis au
%territoire mauricien d'être reconnu internationalement pour son grand essor. Un article de l'éditorial français \href{https://www.contrepoints.org}{Contrepoints} était particulièrement élogieux par rapport au virage économique récent de
%l'île : 
%
%\emph{\og [\ldots{}] Délaissant la culture de la canne à sucre peu rémunératrice, les autorités mauriciennes ont multiplié les initiatives pour faire de cette île une destination recherchée des entrepreneurs, un centre d'externalisation majeur dans cette partie du monde, en favorisant notamment les dispositions et infrastructures liées à l'émergence des Business Process Outsourcing (BPO). Résultat, le secteur tertiaire représente actuellement plus de $70 \%$ du PIB de l'île, et le secteur financier mauricien en plein essor est dorénavant reconnu à travers le monde pour sa compétitivité \fg{}\footnote{https://www.contrepoints.org/2013/11/21/147022-lile-maurice-un-modele-pour-le-continent-africain}.}
%
%Ce n'est d'ailleurs pas par hasard que les histoires économiques à succès se multiplient ces dernières années sur ce territoire. L'économie florissante, l'évolution des mœurs et l'abondance du tourisme
%international sont autant de facteurs permettant à différentes entreprises de rencontrer le succès sur cette île paradisiaque. 
%
%\subsection{Concurrence et opportunités}
%\label{subsec:concurrence_et_opp}
%
%La compagnie PhoenixBev's est un succès commercial d'importance sur l'île Maurice. La Phœnix est une bière locale commercialisée par \textsc{Phoenix Beverages Group} depuis 1963. Aujourd'hui, l'entreprise lauréate d'une multitude de prix internationaux emploie plus de 1 200 Mauriciens et compte 3 usines de production à Maurice.  Parallèlement à cet impressionnant succès commercial qu'est Phoenix, un petit joueur -- \textsc{Flying Dodo} -- conscient du marché local en pleine ébullition, démarre des activités brassicoles sur l'île au tournant des années 2010. Les commentaires du propriétaire de l'entreprise -- Oscar Olsen -- sont enthousiasmants quant au potentiel de l'île Maurice en matière de développement du marché de la bière : \og \emph{If I was living in Europe this project of mine would have been difficult because it exists already there. "And that is the nice thing with Africa---it is still, for a lot of things, virgin territory, and those with ideas that are willing to do something can achieve a lot of things here,"}\fg{} dit M. Olsen\footnote{\url{https://www.bbc.com/news/business-28837758}}. Il renchérit d'ailleurs en parlant plus spécifiquement du territoire de
%l'île Maurice : \og \emph{ Mauritius I'd say is a good place to start a business. Of course we have hurdles, and there are lots of things that us Mauritians get really irritated with but overall I'm quite happy to do it here.} \fg{}\footnote{Ibid}
%
%\subsection{Évolution des revenus/habitants et part du revenu consacré à  la bière}
%\label{subsec:evol_revenus}
%
%La \autoref{wrap-fig:2} illustre l'évolution positive du pouvoir d'achat des Mauriciens par rapport à la très grande majorité des pays de l'Afrique subsaharienne. 
%En effet, à l'exception des Seychelles, l'île Maurice est l'État africain ayant vu son pouvoir d'achat croître le plus au cours des dernières années, lui permettant d'occuper le deuxième rang de toute l'Afrique avec un PIB/habitant d'environ 23 000 USD/habitant.
%
%\begin{wrapfigure}{r}{0.8\textwidth}
%		\includegraphics[width=0.75\textwidth]{images/1/2}
%		\caption{PIB/habitant en parité de pouvoir d'achat ajusté pour l'inflation}\label{wrap-fig:2}
%\end{wrapfigure} 
%Qui plus est, la tendance est positive et la croissance tend à s'accélérer depuis ces dernières années. Il s'agit d'un indicateur de santé économique très encourageant.
%
%Par ailleurs, si on s'intéresse plus spécifiquement à la part de la bière dans le panier de consommation des Mauriciens, on constate une prévalence intéressante de ce breuvage d'un point de vue économique pour \textsc{BeerHaven}. En effet, de l'ensemble des biens consommés par les Mauriciens, la \autoref{wrap-fig:3} nous informe que $11 \%$ de ceux-ci sont de l'alcool ou du tabac. La bière représente d'ailleurs $27 \%$ de cette catégorie. Nous pourrions donc extrapoler et comprendre que la bière représente $3 \%$ des biens de consommation des habitants de l'île Maurice. Une statistique significative et importante à retenir pour la suite de notre analyse\footnote{Statistics Mauritius : "Household budget survey 2017 and The updated consumer price index --  Methodological Report''}. 
%
%\newpage
%
%\begin{wrapfigure}{r}{0.4\textwidth}
%		\includegraphics[width=0.37\textwidth]{images/1/3}
%		\caption{Division des dépenses de consommation}\label{wrap-fig:3}
%\end{wrapfigure} 
%On constate en outre sur la \autoref{wrap-fig:4} que les boissons alcoolisées et le tabac sont passés de $9,6 \%$ à $11 \%$ du panier de consommation mauricien de 2012 à 2017 et que la bière au sein de ce groupe spécifique a vu sa pondération évoluer de $22 \%$ à $27 \%$, indiquant clairement une évolution de la consommation de la bière sur le territoire mauricien ces dernières années.
%\begin{wrapfigure}{l}{0.6\textwidth}
%		\includegraphics[width=0.57\textwidth]{images/1/4}
%		\caption{Détail des boissons alcoolisées et du tabac}\label{wrap-fig:4}
%\end{wrapfigure} 

\end{document}